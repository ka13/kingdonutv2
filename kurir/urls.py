from django.contrib import admin
from django.urls import path, include
from kurir import views

app_name = 'kurir'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('kurir/', views.kurir, name='kurir'),
  ]