from django.test import TestCase, Client
from .models import Person1

# Create your tests here.
class group_test(TestCase):

    def test_url_home(self):
        self.assertEqual(Client().get('').status_code, 200)
    
    def test_url_order(self):
        self.assertEqual(Client().get('/order/').status_code, 200)
    
    def test_url_signup(self):
        self.assertEqual(Client().get('/signup/').status_code, 200)
    
    def test_url_login(self):
        self.assertEqual(Client().get('/login/').status_code, 200)


    # def test_model_person(self):
    #     Person1.objects.create(name='F', number='0', address='A', donat_flav_A=1)
    #     count = Person1.objects.all().count()
    #     self.assertEqual(count, 1)

    def test_home_template(self):
        self.assertTemplateUsed(Client().get(''), 'home.html')
    
    def test_order_template(self):
        self.assertTemplateUsed(Client().get('/order/'), 'schedule.html')

    def test_signup_template(self):
        self.assertTemplateUsed(Client().get('/signup/'), 'signup.html')
    
    def test_login_template(self):
        self.assertTemplateUsed(Client().get('/login/'), 'login.html')