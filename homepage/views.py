from django.shortcuts import render, redirect
from . import forms
from .forms import UserCreateForm
from .models import Person1
from django.core import serializers
from django.http import HttpResponse
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.contrib.auth import login, logout, authenticate


# Create your views here.
def home(request):
    return render(request, "home.html")

def person_create(request):
    if request.method == "POST":
        form = forms.Person(request.POST)
        if form.is_valid():   
            new_sched = form.save(commit=False)
            new_sched.name = request.user.username
            new_sched.save()
            return redirect('homepagee:post_list')
    else:
        form = forms.Person()

    return render(request, "schedule.html", {"form" : form, "person": Person1.objects.all().order_by("date")})

def getData(request):
    person = Person1.objects.all()
    person_list = serializers.serialize('json', person)
    return HttpResponse(person_list, content_type="text/json-comment-filtered")

def login1(request):
    if request.method == "POST":
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            return redirect('homepage:home')
    else:
        form = AuthenticationForm()
    return render(request, 'login.html', {'form': form})

def logout1(request):
    logout(request)
    return redirect('homepage:home')

def signup(request):
    if request.method == 'POST':
        form = UserCreateForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('homepage:home')
    else:
        form = UserCreateForm()
    return render(request, 'signup.html', {'form': form})
