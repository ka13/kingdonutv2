from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.home, name='home'),
    path('order/', views.person_create, name="person"),
    path('getData/', views.getData, name="get-data"),
    path('login/', views.login1, name="login"),
    path('signup/', views.signup, name="signup"),
    path('logout/', views.logout1, name="logout"),
]