from django.db import models
import django.utils.timezone


# Create your models here.
class Person1(models.Model):

    name = models.CharField(max_length=100, null=True, blank=True)
    number = models.CharField(max_length=100)
    address = models.CharField(max_length=100)
   
    date = models.DateField(auto_now_add = False, default = django.utils.timezone.now)

    BASE = [
        ('Choco', 'Choco'),
        ('Mix', 'Mix'),
        ('Original', 'Original')
    ]

    donut_base = models.CharField(choices=BASE, default='Original', max_length=10)

    donut_flav_A = models.DecimalField(default=0, max_digits=2, decimal_places=0)
    donut_flav_B = models.DecimalField(default=0, max_digits=2, decimal_places=0)
    donut_flav_C = models.DecimalField(default=0, max_digits=2, decimal_places=0)
    donut_flav_D = models.DecimalField(default=0, max_digits=2, decimal_places=0)
    donut_flav_E = models.DecimalField(default=0, max_digits=2, decimal_places=0)
    donut_flav_F = models.DecimalField(default=0, max_digits=2, decimal_places=0)
    donut_flav_G = models.DecimalField(default=0, max_digits=2, decimal_places=0)
    donut_flav_H = models.DecimalField(default=0, max_digits=2, decimal_places=0)

    def __str__(self):
        return self.name

# class Person1(models.Model):

    # name = models.CharField(max_length=100)
    # number = models.DecimalField(max_digits=100, decimal_places=0)
    # address = models.CharField(max_length=100)
   
    # date = models.DateField(auto_now_add = False, default = django.utils.timezone.now)
    # day = models.CharField(max_length=10, default=None, null=True)
    # time = models.TimeField(auto_now_add = False, default = django.utils.timezone.now)

    # BASE = [
    #     ('Choco', 'Choco'),
    #     ('Mix', 'Mix'),
    #     ('Original', 'Original')
    # ]

    # donut_base = models.CharField(choices=BASE, default='Original', max_length=10)
    
    # donut_flav_A = models.DecimalField(max_digits=2, decimal_places=0)
    # donut_flav_B = models.DecimalField(max_digits=2, decimal_places=0)
    # donut_flav_C = models.DecimalField(max_digits=2, decimal_places=0)
    # donut_flav_D = models.DecimalField(max_digits=2, decimal_places=0)
    # donut_flav_E = models.DecimalField(max_digits=2, decimal_places=0)
    # donut_flav_F = models.DecimalField(max_digits=2, decimal_places=0)
    # donut_flav_G = models.DecimalField(max_digits=2, decimal_places=0)
    # donut_flav_H = models.DecimalField(max_digits=2, decimal_places=0)

    # def __str__(self):
    #     return self.title
