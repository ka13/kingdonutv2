from django import forms
from . import models
from django.contrib.auth.models import User
from django.utils import timezone
from datetime import datetime
from django.contrib.auth.forms import UserCreationForm

class Time(forms.TimeInput):
    input_type = "time"

class Date(forms.DateInput):
    input_type = "date"
    input_formats = ['%d %b %Y']

class Person(forms.ModelForm):
    class Meta:
        model = models.Person1
        fields = ['name', 'number', 'address', 'date', 'donut_base', 'donut_flav_A', 'donut_flav_B', 'donut_flav_C', 'donut_flav_D', 'donut_flav_E', 'donut_flav_F', 'donut_flav_G', 'donut_flav_H']

        widgets = {
            "date": Date
        }
        
class UserCreateForm(UserCreationForm):
    password1 = forms.CharField (
                label="Password",
                widget=forms.PasswordInput(attrs={'placeholder':'At least 8 characters'}),
                min_length=8,
                help_text=None
    )
    password2 = forms.CharField (
                label="Confirm Password",
                widget=forms.PasswordInput(attrs={'placeholder':'Re-enter password'}),
                min_length=8,
                help_text=None
    )
    class Meta:
        model = User
        fields = (
        'username',
        'password1',
        'password2'
        )
        help_texts = {
            'username':None,
            'password1':None,
            'password2':None
        }