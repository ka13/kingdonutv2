from django.shortcuts import render, redirect
from django.http import HttpResponse
from homepage.models import Person1
from django.urls import reverse_lazy
from django.views.generic.edit import DeleteView
from homepagee.forms import PostForm
from homepagee.models import Post
from django.core import serializers
# Create your views here.
def post_list(request):
    return render(request, 'post_list.html', {'postlist': Person1.objects.all()})
    
def feedback(request):
    postlist = Post.objects.all()
    return render(request, 'feedback.html', {'postlist2':postlist, 'postlist3' : Person1.objects.all()})
    
    
def post_new(request):
    if request.method == "POST":
           form = PostForm(request.POST)
           if form.is_valid():
               post = form.save(commit=False)
               post.nama=request.user
               post.save()
               return redirect('/feedback/')
    else:
        form = PostForm()
    return render(request, 'bayar.html', {'form': form})
    
    
class Delete(DeleteView):
    model=Person1
    success_url=reverse_lazy('homepagee:post_list')
    
def getDataFeedback(request):
    feedback=Post.objects.all()
    feedback_list=serializers.serialize('json', feedback)
    return HttpResponse(feedback_list, content_type="text/json-comment-filtered")
    