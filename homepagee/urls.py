from django.urls import path
from . import views
from django.conf.urls import url

app_name = 'homepagee'

urlpatterns = [
    path('show/', views.post_list, name='post_list'),
    
    path('bayar/', views.post_new, name='bayar'),
    path('feedback/', views.feedback, name='feedback'),
    path('getDataFeedback/', views.getDataFeedback, name='getDataFeedback'),
    
    url(r'show/(?P<pk>[0-9]+)/delete/$', views.Delete.as_view(),name='delete'),
]