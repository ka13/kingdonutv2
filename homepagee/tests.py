from django.test import TestCase, Client
from django.urls import resolve
from homepagee.views import post_list, feedback, post_new 
from homepagee.models import Post
from homepagee.forms import PostForm
from django.http import HttpRequest

# Create your tests here.
class HomepageeUnitTest(TestCase):

	@classmethod
	def setUpTestData(cls):
		Post.objects.create(nama="Nazila", feedback="Recommended")

	# Test urls and views

	def test_show_url_is_exist(self):
		response = Client().get('/show/')
		self.assertEqual(response.status_code, 200)

	def test_bayar_url_is_exist(self):
		response = Client().get('/bayar/')
		self.assertEqual(response.status_code, 200)

	def test_feedback_url_is_exist(self):
		response = Client().get('/feedback/')
		self.assertEqual(response.status_code, 200)

	def test_show_using_postlist_func(self):
		found = resolve('/show/')
		self.assertEqual(found.func, post_list)

	def test_feedback_using_feedback_func(self):
		found = resolve('/feedback/')
		self.assertEqual(found.func, feedback)

	def test_bayar_using_postnew_func(self):
		found = resolve('/bayar/')
		self.assertEqual(found.func, post_new)


	# Test models

	def test_if_models_in_database(self):
		feedbackData = Post.objects.create(nama="Nazil", feedback="recommended bgt")
		count_feedbackData = Post.objects.all().count()
		self.assertEqual(count_feedbackData, 2)

	def test_if_feedbackData_nama_is_exist(self):
		feedbackDataObj = Post.objects.get(id=1)
		namaObj = feedbackDataObj._meta.get_field('nama').verbose_name
		self.assertEqual(namaObj, 'nama')

	def test_if_feedbackData_feedback_is_exist(self):
		feedbackDataObj = Post.objects.get(id=1)
		feedbackObj = feedbackDataObj._meta.get_field('feedback').verbose_name
		self.assertEqual(feedbackObj, 'feedback')

	# Test form

	def test_form_input_html(self):
		form = PostForm()
		self.assertIn('id="id_feedback', form.as_p())

	def test_form_validation_blank(self):
		form = PostForm(data={'feedback':''})
		self.assertFalse(form.is_valid())
		self.assertEquals(form.errors['feedback'], ["This field is required."])

	# Test HTML

	def test_feedback_page_using_feedback_template(self):
		response = Client().get('/feedback/')
		self.assertTemplateUsed(response, 'feedback.html')

	def test_bayar_page_using_bayar_template(self):
		response = Client().get('/bayar/')
		self.assertTemplateUsed(response, 'bayar.html')

	def test_show_page_using_postlist_template(self):
		response = Client().get('/show/')
		self.assertTemplateUsed(response, 'post_list.html')