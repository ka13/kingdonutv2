from django.apps import AppConfig


class HomepageeConfig(AppConfig):
    name = 'homepagee'
