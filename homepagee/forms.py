from django import forms
from .models import Post

class PostForm(forms.ModelForm):

    class Meta:
        model = Post
        fields = ['feedback']
        
        
        
    def __init__(self, *args, **kwargs):
        super(PostForm,self).__init__(*args,**kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class':'form-control'
            })
        