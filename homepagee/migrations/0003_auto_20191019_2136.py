# Generated by Django 2.2.5 on 2019-10-19 14:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('homepagee', '0002_post_nama'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='nama',
            field=models.CharField(default='', max_length=30),
        ),
    ]
