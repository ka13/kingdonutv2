from django import forms
from . import models

class RequestBahan(forms.ModelForm):
	class Meta:
		model = models.Bahan
		fields = ['nama', 'bahanReq']