from django.shortcuts import render,redirect
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from homepage.models import Person1
from django.core import serializers
from .models import Bahan
from . import forms

# Create your views here.
def chef(request):
	response = {}
	pesanan1 = Person1.objects.all().order_by('date')
	response['pesanan_list'] = pesanan1
	return render(request, "chef.html", response)

def list_bahan(request):
	response = {}
	bahan_list = Bahan.objects.all()
	response['bahan_bahan'] = bahan_list
	return render(request, 'list_bahan.html', response)

def bahan(request):
	if request.method == 'POST':
		form = forms.RequestBahan(request.POST, request.FILES)
		if form.is_valid():
			instance = form.save(commit=False)
			form.save()
			return redirect('chef:bahan')
	else:
		form = forms.RequestBahan()
	return render(request, 'request_bahan.html', {'form' : form})

def req_done(request, id):
    if request.method == 'POST':
	    Bahan.objects.filter(id=id).delete()
	    return redirect('chef:list_bahan')
    else:
    	return HttpResponse("/GET not allowed")


def getPesanan(request):
    dataBahan = Bahan.objects.all()
    listBahan = serializers.serialize('json', dataBahan)
    return HttpResponse(listBahan, content_type="text/json-comment-filtered")

