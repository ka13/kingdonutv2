from django.contrib import admin
from django.urls import path, include
from chef import views

app_name = 'chef'

urlpatterns = [
	path('admin/', admin.site.urls),
	path('chef/', views.chef, name='chef'),
	path('chef/request/', views.bahan, name='bahan'),
	path('list/bahan/', views.list_bahan, name='list_bahan'),
	path('list/bahan/done/<int:id>/', views.req_done, name='req_done'),
	path('getPesanan/', views.getPesanan, name='getPesanan'),
	]
