from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.apps import apps
from django.http import HttpRequest
from .models import Bahan
from .forms import RequestBahan
from .views import chef, bahan

# Create your tests here.
class UnitTest(TestCase):
	
	# tests for url and views
	def test_chef_url_exist(self):
		response = Client().get('/chef/')
		self.assertEqual(response.status_code, 200)

	def test_request_bahan_url_exist(self):
		response = Client().get('/chef/request/')
		self.assertEqual(response.status_code, 200)

	def test_list_bahan_url_exist(self):
		response = Client().get('/list/bahan/')
		self.assertEqual(response.status_code, 200)

	def test_chef_using_chef_func(self):
		found = resolve('/chef/')
		self.assertEqual(found.func, chef)

	def test_request_bahan_using_request_bahan_func(self):
		found = resolve('/chef/request/')
		self.assertEqual(found.func, bahan)


	# tests for models
	def test_models_in_database(self):
		data_bahan = Bahan.objects.create(nama="Natasya", bahanReq="gula, tepung, telur")
		count_data = Bahan.objects.all().count()
		self.assertEqual(count_data, 1)

	def create_data(self, nama="Natel", bahanReq="gula dan tepung"):
		return Bahan.objects.create(nama=nama,bahanReq=bahanReq)

	def test_create_data_creation(self):
		d = self.create_data()
		self.assertTrue(isinstance(d, Bahan))


	# tests for forms
	def test_form_input(self):
		bahanReq = RequestBahan()
		self.assertIn('id="id_nama"', bahanReq.as_p())
		self.assertIn('id="id_bahanReq"', bahanReq.as_p())

	def test_bahanReq_validation_if_blank(self):
		nama_bahanReq = RequestBahan(data={'nama':'', 'bahanReq':''})
		self.assertFalse(nama_bahanReq.is_valid())
		self.assertEqual(nama_bahanReq.errors['nama'], ["This field is required."])

	# test post
	def test_bahan_post_fail(self):
		response = Client().post('/chef/request/', {'nama': 'Anonymous', 'bahanReq' : 'gula'})
		self.assertEqual(response.status_code, 302)

	def test_list_bahan_post_success_and_render_the_result(self):
		nama = 'nama'
		bahanReq = 'bahanReq'
		response = Client().post('/chef/request/', {'nama':'', 'bahanReq':''})
		self.assertEqual(response.status_code, 200)
		html_response = response.content.decode('utf8')
		self.assertIn(nama, html_response)
		self.assertIn(bahanReq, html_response)